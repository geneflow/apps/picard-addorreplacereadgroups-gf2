Picard AddOrReplaceReadGroups GeneFlow App
==========================================

Version: 2.22.7-03

This GeneFlow app wraps the Picard AddOrReplaceReadGroups tool.

Inputs
------

1. input: BAM File - An alignment BAM file.

Parameters
----------

1. id: Read group ID, default: 1

2. library: Read group library.

3. platform: Read group platform.

4. barcode: Read group platform unit (i.e., barcode).

5. sample: Read group sample name.

6. output: Output directory - The name of the output directory to place BAM file. Default: output.

